import numpy as np
import statistics
import matplotlib.pyplot as plt
import csv
import chain_different_linknumber as mux

##################################################
# ENTERING PARAMETERS, RUNNING THE SIMULATION    #
# AND COLLECT DATA POINTS                        #
##################################################

# simulation parameters

number_of_blocks = 6
elementary_link_attempt_success_probability = 0.01
device_initial_failure_probability = 0
device_failure_probability_per_timestep = 0 
coherence_time = 1500  # timesteps
number_of_simulation_rounds = 10

#chain multiplexing structure, i.e. numbers of multiplexed devices per block
chains = [[1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 2], [1, 1, 1, 1, 1, 3], [1, 1, 1, 1, 2, 2], [1, 1, 1, 1, 2, 3], [1, 1, 1, 1, 3, 3], [1, 1, 1, 2, 2, 2], [1, 1, 1, 2, 2, 3], [1, 1, 1, 2, 3, 3], [1, 1, 1, 3, 3, 3], [1, 1, 2, 2, 2, 2], [1, 1, 2, 2, 2, 3], [1, 1, 2, 2, 3, 3], [1, 1, 2, 3, 3, 3], [1, 1, 3, 3, 3, 3], [1, 2, 2, 2, 2, 2], [1, 2, 2, 2, 2, 3], [1, 2, 2, 2, 3, 3], [1, 2, 2, 3, 3, 3], [1, 2, 3, 3, 3, 3], [1, 3, 3, 3, 3, 3], [2, 2, 2, 2, 2, 2], [2, 2, 2, 2, 2, 3], [2, 2, 2, 2, 3, 3], [2, 2, 2, 3, 3, 3], [2, 2, 3, 3, 3, 3], [2, 3, 3, 3, 3, 3], [3, 3, 3, 3, 3, 3]]


#files in which the data get stored
files = ["data_c0.csv","data_c1.csv","data_c2.csv","data_c3.csv","data_c4.csv", "data_c5.csv","data_c6.csv","data_c7.csv","data_c8.csv","data_c9.csv", "data_c10.csv","data_c11.csv","data_c12.csv","data_c13.csv","data_c14.csv", "data_c15.csv","data_c16.csv","data_c17.csv","data_c18.csv","data_c19.csv", "data_c20.csv","data_c21.csv","data_c22.csv","data_c23.csv","data_c24.csv", "data_c25.csv","data_c26.csv","data_c27.csv"]




# shortening names so that lines don't become longer than 80 lines,
# to comply with the PEP8 style of writing Python

initial_fail_prob = device_initial_failure_probability
variable_fail_prob = device_failure_probability_per_timestep
succ_prob = elementary_link_attempt_success_probability





#loop over graph structures
for c in range(0,len(chains)):
    print("chain", c)
    number_of_devices_per_block = chains[c]
    num_conns = number_of_devices_per_block
    #print(num_conns)

    # create repeater chain from simulation parameters
    repeater_chain = \
        mux.create_repeater_chain(
            connection_attempt_success_probability=succ_prob,
            connection_initial_failure_probability=initial_fail_prob,
            number_of_connections_per_segment=num_conns,
            connection_failure_probability_per_timestep=variable_fail_prob,
            coherence_time=coherence_time,
            number_of_segments=number_of_blocks)

    #loop over simulation rounds
    for round_number in range(1, number_of_simulation_rounds + 1):
        repeater_chain.perform_new_round()

        """Daten in csv schreiben"""  
        with open(files[c], mode='a') as csvfile:
            csvfile_writer = csv.writer(csvfile, delimiter=',')
            csvfile_writer.writerow([succ_prob,coherence_time,
                repeater_chain.waiting_time, 
                repeater_chain.fidelity, c, num_conns])

