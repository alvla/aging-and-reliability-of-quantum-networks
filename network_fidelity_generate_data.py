import numpy as np
import statistics
import matplotlib.pyplot as plt
import csv
import network_fidelity as mux

##################################################
# ENTERING PARAMETERS, RUNNING THE SIMULATION    #
# AND COLLECT DATA POINTS                        #
##################################################

# simulation parameters

number_of_blocks = 6
number_of_devices_per_block = 1
elementary_link_attempt_success_probability = 0.01
device_initial_failure_probability = 0
device_failure_probability_per_timestep = 0
coherence_time = 1500  # timesteps
number_of_simulation_rounds = 10


#network structure, i.e. all paths of a graph which connects end points
graph0 = [[0,2,4,5]]                            #network 0) aceg
graph1 = [[0,3,5]]                              #network 1) adg
graph2 = [[0,3,5],[1,2,3,5]]                    #network 2) adg bcdg
graph3 = [[0,3,5],[1,4,5]]                      #network 3) adg beg
graph4 = [[0,3,5],[0,2,4,5], [1,2,3,5],[1,4,5]] #network 4) adg aceg bcdg beg

graphs = [graph0,graph1,graph2,graph3,graph4]

files = ["data_g0.csv","data_g1.csv","data_g2.csv","data_g3.csv","data_g4.csv"]

# shortening names so that lines don't become longer than 80 lines,
# to comply with the PEP8 style of writing Python

initial_fail_prob = device_initial_failure_probability
variable_fail_prob = device_failure_probability_per_timestep
num_conns = number_of_devices_per_block
succ_prob = elementary_link_attempt_success_probability





#loop over graph structures
for g in range(0,len(graphs)):
    print("graph", g)
    graph = graphs[g]

    # create repeater network from simulation parameters
    repeater_network = \
        mux.create_repeater_network(
            device_attempt_success_probability=succ_prob,
            device_initial_failure_probability=initial_fail_prob,
            number_of_devices_per_block=num_conns,
            device_failure_probability_per_timestep=variable_fail_prob,
            coherence_time=coherence_time,
            number_of_blocks=number_of_blocks,
            graph=graph)

    #loop over simulation rounds
    for round_number in range(1, number_of_simulation_rounds + 1):
        repeater_network.perform_new_round()

        """Daten in csv schreiben"""  
        with open(files[g], mode='a') as csvfile:
            csvfile_writer = csv.writer(csvfile, delimiter=',')
            csvfile_writer.writerow([succ_prob,coherence_time,
                repeater_network.waiting_time, 
                repeater_network.fidelity, g, graphs[g]])

