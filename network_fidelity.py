"""Monte Carlo simulation of a multiplexed repeater network with defects.



Code explanation
================

The architecture
----------------

The repeater network has a similar architecture as the chain. 
It consist of as many blocks as the network has. It needs to be defined, 
which paths build a successfull connection.



Each entangled pair is modelled as a Werner state
:math:`\rho(w) = w \Psi^- + (1 - w) Id_4` where :math:`w` is the Werner
parameter and :math:`\Psi^-` is a perfect Bell state.
The Bell state fidelity :math:`F` is related to Bell-state fidelity
as :math:`F = (1 + 3 w) / 4`.

An entanglement swap on two Werner states :math:`\rho(w_A), \rho(w_B)`
yields a Werner state :math:`\rho(w_A \cdot w_B)`.

Stored entanglement decays from :math:`\rho(w)` to
:math:`\rho(w\cdot e^{-t / T_{coh}}` where :math:`T_{coh}` is the
coherence time of the memories.


The protocol
------------

The protocol that this script currently implements is as follows: over each
device, elementary-link generation is attempted once per timestep.
As soon as *at least one* of the elementary links within a single block
has succeeded, this single elementary link is kept and the other devices
stop attempting generating entanglement or discard any entanglement they
might have generated within the same timestep as the succeeding attempt
(Note: this is not optimal; it would be better to keep entangling and
use only the latest generated link since that would yield less state decay).

As soon as there is a path with a single elementary link on each block, an entanglement
swap is performed by each node that is not an end node, yielding entanglement
between the end nodes.
"""
import numpy as np
import statistics
import matplotlib.pyplot as plt
import csv


#################################################
# CLASSES FOR A DEVICE, BLOCK, REPEATERNETWORK; #
# - a block holds multiple devices              #
# - a repeater network holds multiple blocks    #
#################################################


class Device:
    """Class modelling an elementary link/a device.
    Intended usage: after initialization, one can call
    `perform_new_round` to perform entanglement generation
    over this device until success, and query `waiting_time`
    after that to find out how long that took. The function
    `apply_failing` can be used in between to probabilistically
    turn the device "off", i.e. ensure it will not generate
    entanglement at all any more. In case the device
    is turned off, the waiting time will be infinitely long
    (i.e. :obj:`numpy.inf`).

    Parameters
    ----------
    attempt_success_probability: float
        The probability of successfully generating an
        elementary link in a single attempt
    is_working: bool
        Whether the device is defect
    failure_probability_per_timestep: float
    """

    def __init__(self,
                 attempt_success_probability=1,
                 is_working=True,
                 failure_probability_per_timestep=0):
        self.is_working = is_working
        self.attempt_success_probability = attempt_success_probability
        self._waiting_time = None
        self.failure_probability_per_timestep = \
            failure_probability_per_timestep

    @property
    def waiting_time(self):
        if self._waiting_time is None:
            raise Exception("First call 'perform_new_round' on the device")
        else:
            return self._waiting_time

    def perform_new_round(self):
        """Models performing an entanglement generation round: discard
        any entanglement present and performs entanglement generation
        until success; then sets the waiting time according to the time
        that process took.

        Includes a call to `apply_failing` during elementary-link-generation
        to probabilistically turn the device "off", i.e. ensure it will not
        generate entanglement at all any more (in which case it has become
        defect).
        """
        self._waiting_time = self._sample_waiting_time()

    def _sample_waiting_time(self):
        has_finished = False
        time = 0
        while not has_finished and self.is_working:
            self.apply_failing(
                failure_probability=self.failure_probability_per_timestep)
            if np.random.random() < self.attempt_success_probability:
                # succeeded generating elementary link
                has_finished = True
            time += 1

        if self.is_working:
            waiting_time = time
        else:
            waiting_time = np.inf
        return waiting_time

    def turn_off(self):
        self.is_working = False

    def apply_failing(self, failure_probability=0):
        """Throws a random coin to determine whether to turn it to
        'defect'. If the device already was defect it will remain so.
        """
        if np.random.random() < failure_probability:
            self.turn_off()

    @property
    def initial_werner_parameter(self):
        """The quality of the generated entanglement before decoherence
        over time."""
        # NOTE currently hardcoded: werner parameter =1, i.e.
        # a perfect Bell state
        return 1.0


class Block:
    """Class modelling a block, i.e. a collection of
    elementary links/devices between a given pair of nodes.

    Intended usage: after initialization, one can call
    `perform_new_round` to perform entanglement generation
    over this block until the first device succeeds.
    After that, query `waiting_time` after that to find out how long
    that took. The function `perform_new_round` includes probabilistically
    turning the individual devices off (i.e. the devices become
    defects).

    Parameters
    ----------
    devices: list of :obj:`Device`
    device_failure_probability_per_timestep: float
    coherence_time: float
    """

    def __init__(self,
                 devices,
                 coherence_time=np.inf):
        self.devices = devices
        self.coherence_time = coherence_time
        self._waiting_time = None
        self._initial_werner_parameter_of_succeeding_device = None

    @property
    def waiting_time(self):
        if self._waiting_time is None:
            raise Exception("First call 'perform_new_round' on the block")
        else:
            return self._waiting_time

    def werner_parameter(self, time):
        """Werner parameter of the earliest succeeding link at time `time`.
        The werner parameter is computed from the initial werner parameter
        (representing the quality of the entangled pair when it has just been
        generated) and a decay factor dependent on the difference of `time`
        and the time at which the pair was generated.

        Parameters
        ----------
        time: float

        Returns
        -------
        float or None
            None if no entanglement is present (because the block is defect).
        """
        if self._initial_werner_parameter_of_succeeding_device is None:
            raise Exception("First call 'perform_new_round' on the block")
        else:
            initial = self._initial_werner_parameter_of_succeeding_device
            assert(time >= self._waiting_time)
            delta_t = time - self._waiting_time
            if delta_t < np.inf:
                return initial * np.exp(- delta_t / self.coherence_time)
            else:
                return None

    def perform_new_round(self):
        """Perform a new round of entanglement generation, i.e. this method
        models discarding any existing entanglement, have each device
        generate entanglement until the first of them is finished,
        and update the `waiting_time` as the time this took.

        Includes a call to `apply_failing` during elementary-link-generation
        to probabilistically turn the individual devices "off",
        i.e. ensure they will not generate entanglement at all any more
        (they become defect).
        """
        for device in self.devices:
            device.perform_new_round()

        # determine the device which succeeded
        earliest_device = self.devices[0]
        for device in self.devices:
            if device.waiting_time < earliest_device.waiting_time:
                earliest_device = device

        self._waiting_time = earliest_device.waiting_time
        self._initial_werner_parameter_of_succeeding_device = \
            earliest_device.initial_werner_parameter

    def apply_failing(self, device_failure_probability=0):
        """Throws a random coin for each device to determine
        whether to turn it to 'defect'. Devices which already
        were defect will remain so.

        Parameters
        ----------
        device_failure_probability: float
        """
        for device in self.devices:
            device.apply_failing(
                failure_probability=device_failure_probability)

    @property
    def status(self):
        """Returns a string of 'o' and 'x', e.g.
        'ooxxo' indicates the 3rd and 4th device
        are defect.
        """
        string = ""
        for device in self.devices:
            if device.is_working:
                string += "o"
            else:
                string += "x"
        return string


class RepeaterNetwork:
    """Class modelling a network of blocks.

    Parameters
    ----------
    blocks: list of :obj:`Block`
    graph: vecotr of vectors
    shortest_path: vector
    """

    def __init__(self, blocks,graph):
        self.blocks = blocks
        self._waiting_time = None
        self.graph = graph
        self.shortest_path = graph[0]
        self.werner_parameter = None
        self.data = []

    @property
    def waiting_time(self):
        if self._waiting_time is None:
            raise Exception(
                "First call 'perform_new_round' on the repeater network")
        else:
            return self._waiting_time

    def perform_new_round(self):
        for block in self.blocks:
            block.perform_new_round()
            #print(block.waiting_time)
            #print(self.blocks[0].waiting_time)
        self.shortest_path = self.graph[0]
        self._waiting_time = max([self.blocks[dev].waiting_time
                                 for dev in self.shortest_path])
        self.werner_parameter = self.compute_werner_parameter(path = self.shortest_path)
        #print(self.shortest_path)
        #print("werner parameter: ", self.werner_parameter)
        #print("waiting time path 0: ", self._waiting_time)
        if len(self.graph) > 1:
            #print("length of grapht: ", len(self.graph))
            for path in range(1,len(self.graph)):
                path_waiting_time = max([self.blocks[dev].waiting_time
                                         for dev in self.graph[path]])
                #print("waiting time path", path, ": ", path_waiting_time)
                if path_waiting_time < self._waiting_time:
                    self._waiting_time = path_waiting_time
                    self.shortest_path = self.graph[path]
                    self.werner_parameter = self.compute_werner_parameter(path = self.shortest_path)
                    #print(self.shortest_path)
                    #print("werner parameter: ", self.werner_parameter)
                elif path_waiting_time == self._waiting_time:
                    #print("equal")
                    path_werner_parameter = self.compute_werner_parameter(path = self.graph[path])
                    #print("path werner parameter: ", path_werner_parameter)
                    if path_werner_parameter > self.werner_parameter:
                        self.shortest_path = self.graph[path]
                        self.werner_parameter = path_werner_parameter
                        #print("werner parameter: ", self.werner_parameter)
        self.data.append([self._waiting_time, self.fidelity])
                        


        #print(self._waiting_time)
        #self.blocks[0].waiting_time  


    def apply_failing(self, device_failure_probability=0):
        for block in self.blocks:
            block.apply_failing(
                device_failure_probability=device_failure_probability)

    @property
    def status(self):
        string = ""
        for block in self.blocks:
            string += block.status
            string += " - "
        return string


    def compute_werner_parameter(self,path):
        """The Werner parameter of the end-to-end link, computed from
        the Werner parameters of the individual elementary links before the
        entanglement swaps. Includes decoherence over time.
        """
        # Return the product of the individual werner parameters
        werner_parameter = 1
        for dev in path:
        #for block in self.blocks:
            w = self.blocks[dev].werner_parameter(time=self.waiting_time) 
            #w = block.werner_parameter(time=100) #xxx Achtung! berechnet werner parameter nach 100 zeitschritten statt nach waiting time.
            if w is None:
                return None
            else:
                werner_parameter *= w
        return werner_parameter

    @property
    def fidelity(self):
        """Bell-state fidelity of the end-to-end link.
        Includes decoherence over time.

        Note: Bell-state fidelity of the Werner state
        :math:`\rho(w) = w \Psi^- + (1 - w) Id_4`
        with :math:`\Psi^-` a Bell state
        equals :math:`(1 + 3 w) / 4`.
        """
        if self.werner_parameter is None:
            return None
        else:
            return (1 + 3 * self.werner_parameter) / 4


#######################################################
# METHODS FOR CREATING A SEGMENT AND A REPEATER CHAIN #
#######################################################

def create_block(device_attempt_success_probability,
                   device_initial_failure_probability,
                   device_failure_probability_per_timestep,
                   coherence_time,
                   number_of_devices_per_block):
    """Create a block and apply initial defects to its devices
    according to `device_initial_failure_probability."""

    devices = []
    fail_prob = device_failure_probability_per_timestep

    for __ in range(number_of_devices_per_block):
        device = Device(
            attempt_success_probability=device_attempt_success_probability,
            is_working=True,
            failure_probability_per_timestep=fail_prob,
            )
        device.apply_failing(
            failure_probability=device_initial_failure_probability)
        devices.append(device)

    return Block(devices=devices,
                   coherence_time=coherence_time)


def create_repeater_network(device_attempt_success_probability,
                          device_initial_failure_probability,
                          number_of_devices_per_block,
                          device_failure_probability_per_timestep,
                          coherence_time,
                          number_of_blocks,
                          graph):
    """Create a repeater network and apply initial defects to its
    individual devices according to
    `device_initial_failure_probability."""

    # shortening names so that lines don't become longer than 80 lines,
    # to comply with the PEP8 style of writing Python
    succ_prob = device_attempt_success_probability
    initial_fail_prob = device_initial_failure_probability
    variable_fail_prob = device_failure_probability_per_timestep
    num_conns = number_of_devices_per_block

    blocks = [
        create_block(
            device_attempt_success_probability=succ_prob,
            device_initial_failure_probability=initial_fail_prob,
            device_failure_probability_per_timestep=variable_fail_prob,
            coherence_time=coherence_time,
            number_of_devices_per_block=num_conns)
        for __ in range(number_of_blocks)]

    repeater_network = RepeaterNetwork(blocks=blocks,graph=graph)
    return repeater_network


