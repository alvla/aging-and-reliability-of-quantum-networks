# Aging and Reliability of Quantum Networks

This repository contains a Monte Calro algorithm to simulate waiting time and fidelity of  multiplexed repeater chains or networks. We aim to find the average fidelity for a certain waiting time in different configurations of chains and networks.
The simulation was used in the paper [Aging and Reliability of Quantum Networks, by
Lisa T. Weinbrenner, Lina Vandré, Tim Coopmans, and Otfried Gühne, arXiv: 2305.19976 (2023)](https://arxiv.org/abs/2305.19976).


## Getting started

### Prerequisites
This software was written for `Python 3`. The following packages are required:
```
numpy, statistics, matplotlib, csv

```

### Installation
Clone the repository, following the [instruction provided by GitLab](https://docs.gitlab.com/ee/user/project/repository/). Or download it using the corresponding menu button.


### Usage

To obtain waiting times and fidelities through simulation of certain chain or network, one has to use the files `..._generate_data.py`. They can be run with the command `python chain_different_linknumber_generate_data.py` or `python network_fidelity_generate_data.py`, respectively. This scripts generate data files which contain the simulation results. The simulation parameters can be changed in the file.

Further it is possible to simulate probabilistic initial failures and failure of devices in each time step. To see an example, run `python chain_different_linknumber.py`.



## Files overview
- `chain_different_linknumber.py` contains functions to obtain waiting times and fidelities through simulation of a multiplexed chain and is used by `chain_different_linknumber_generate_data.py`.
- `network_fidelity.py` contains functions to obtain waiting times and fidelities through simulation of a network and is used by `network_fidelity_generate_data.py`.
- `chain_different_linknumber_generate_data.py` contains the code to execute the simulation of a multiplexed repeater chain. The simulation parameters can be adjusted in this file. 
- `network_fidelity_generate_data.py` contains the code to execute the simulation of a network chain. The simulation parameters can be adjusted in this file. 






