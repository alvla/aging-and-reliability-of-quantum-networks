"""Monte Carlo simulation of a multiplexed repeater chain with defects.


Code explanation
================

The architecture
----------------

The repeater chain looks like this:

A1 +-----------+ B1L   B1R +-----------+ C1L  C1R +-----------+ D1
A2 +-----------+ B2L   B2R +-----------+ C2L  C2R +-----------+ D2
A3 +-----------+ B3L   B3R +-----------+ C3L  C3R +-----------+ D3
A4 +-----------+ B4L   B4R +-----------+ C4L  C4R +-----------+ D4

where e.g. qubit `B3L` is the third qubit in the Left register of node B.
Each `+-----------+` represents an elementary link (a "Connection"),
over which entanglement is generated in discrete attempts, which succeeds
with a fixed probability. The repeater chain consists of multiple segments.
A single segment consists of many elementary links, for example the segment
between nodes B and C:

B1R +-----------+ C1L
B2R +-----------+ C2L
B3R +-----------+ C3L
B4R +-----------+ C4L

Each entangled pair is modelled as a Werner state
:math:`\rho(w) = w \Psi^- + (1 - w) Id_4` where :math:`w` is the Werner
parameter and :math:`\Psi^-` is a perfect Bell state.
The Bell state fidelity :math:`F` is related to Bell-state fidelity
as :math:`F = (1 + 3 w) / 4`.

An entanglement swap on two Werner states :math:`\rho(w_A), \rho(w_B)`
yields a Werner state :math:`\rho(w_A \cdot w_B)`.

Stored entanglement decays from :math:`\rho(w)` to
:math:`\rho(w\cdot e^{-t / T_{coh}}` where :math:`T_{coh}` is the
coherence time of the memories.


The protocol
------------

The protocol that this script currently implements is as follows: over each
connection, elementary-link generation is attempted once per timestep.
As soon as *at least one* of the elementary links within a single segment
has succeeded, this single elementary link is kept and the other connections
stop attempting generating entanglement or discard any entanglement they
might have generated within the same timestep as the succeeding attempt
(Note: this is not optimal; it would be better to keep entangling and
use only the latest generated link since that would yield less state decay).

As soon as there is a single elementary link on each segment, an entanglement
swap is performed by each node that is not an end node, yielding entanglement
between the end nodes.
"""
import numpy as np


#####################################################
# CLASSES FOR A CONNECTION, SEGMENT, REPEATERCHAIN; #
# - a segment holds multiple connections            #
# - a repeater chain holds multiple segments        #
#####################################################


class Connection:
    """Class modelling an elementary link/a connection.
    Intended usage: after initialization, one can call
    `perform_new_round` to perform entanglement generation
    over this connection until success, and query `waiting_time`
    after that to find out how long that took. The function
    `apply_failing` can be used in between to probabilistically
    turn the connection "off", i.e. ensure it will not generate
    entanglement at all any more. In case the connection
    is turned off, the waiting time will be infinitely long
    (i.e. :obj:`numpy.inf`).

    Parameters
    ----------
    attempt_success_probability: float
        The probability of successfully generating an
        elementary link in a single attempt
    is_working: bool
        Whether the connection is defect
    failure_probability_per_timestep: float
    """

    def __init__(self,
                 attempt_success_probability=1,
                 is_working=True,
                 failure_probability_per_timestep=0):
        self.is_working = is_working
        self.attempt_success_probability = attempt_success_probability
        self._waiting_time = None
        self.failure_probability_per_timestep = \
            failure_probability_per_timestep

    @property
    def waiting_time(self):
        if self._waiting_time is None:
            raise Exception("First call 'perform_new_round' on the connection")
        else:
            return self._waiting_time

    def perform_new_round(self):
        """Models performing an entanglement generation round: discard
        any entanglement present and performs entanglement generation
        until success; then sets the waiting time according to the time
        that process took.

        Includes a call to `apply_failing` during elementary-link-generation
        to probabilistically turn the connection "off", i.e. ensure it will not
        generate entanglement at all any more (in which case it has become
        defect).
        """
        self._waiting_time = self._sample_waiting_time()

    def _sample_waiting_time(self):
        has_finished = False
        time = 0
        while not has_finished and self.is_working:
            self.apply_failing(
                failure_probability=self.failure_probability_per_timestep)
            if np.random.random() < self.attempt_success_probability:
                # succeeded generating elementary link
                has_finished = True
            time += 1

        if self.is_working:
            waiting_time = time
        else:
            waiting_time = np.inf
        return waiting_time

    def turn_off(self):
        self.is_working = False

    def apply_failing(self, failure_probability=0):
        """Throws a random coin to determine whether to turn it to
        'defect'. If the connection already was defect it will remain so.
        """
        if np.random.random() < failure_probability:
            self.turn_off()

    @property
    def initial_werner_parameter(self):
        """The quality of the generated entanglement before decoherence
        over time."""
        # NOTE currently hardcoded: werner parameter =1, i.e.
        # a perfect Bell state
        return 1.0


class Segment:
    """Class modelling a segment, i.e. a collection of
    elementary links/connections between a given pair of nodes.

    Intended usage: after initialization, one can call
    `perform_new_round` to perform entanglement generation
    over this segment until the first connection succeeds.
    After that, query `waiting_time` after that to find out how long
    that took. The function `perform_new_round` includes probabilistically
    turning the individual connections off (i.e. the connections become
    defects).

    Parameters
    ----------
    connections: list of :obj:`Connection`
    connection_failure_probability_per_timestep: float
    coherence_time: float
    """

    def __init__(self,
                 connections,
                 coherence_time=np.inf):
        self.connections = connections
        self.coherence_time = coherence_time
        self._waiting_time = None
        self._initial_werner_parameter_of_succeeding_connection = None

    @property
    def waiting_time(self):
        if self._waiting_time is None:
            raise Exception("First call 'perform_new_round' on the segment")
        else:
            return self._waiting_time

    def werner_parameter(self, time):
        """Werner parameter of the earliest succeeding link at time `time`.
        The werner parameter is computed from the initial werner parameter
        (representing the quality of the entangled pair when it has just been
        generated) and a decay factor dependent on the difference of `time`
        and the time at which the pair was generated.

        Parameters
        ----------
        time: float

        Returns
        -------
        float or None
            None if no entanglement is present (because the segment is defect).
        """
        if self._initial_werner_parameter_of_succeeding_connection is None:
            raise Exception("First call 'perform_new_round' on the segment")
        else:
            initial = self._initial_werner_parameter_of_succeeding_connection
            assert(time >= self._waiting_time)
            delta_t = time - self._waiting_time
            if delta_t < np.inf:
                return initial * np.exp(- delta_t / self.coherence_time)
            else:
                return None

    def perform_new_round(self):
        """Perform a new round of entanglement generation, i.e. this method
        models discarding any existing entanglement, have each connection
        generate entanglement until the first of them is finished,
        and update the `waiting_time` as the time this took.

        Includes a call to `apply_failing` during elementary-link-generation
        to probabilistically turn the individual connections "off",
        i.e. ensure they will not generate entanglement at all any more
        (they become defect).
        """
        for connection in self.connections:
            connection.perform_new_round()

        # determine the connection which succeeded
        earliest_connection = self.connections[0]
        for connection in self.connections:
            if connection.waiting_time < earliest_connection.waiting_time:
                earliest_connection = connection

        self._waiting_time = earliest_connection.waiting_time
        self._initial_werner_parameter_of_succeeding_connection = \
            earliest_connection.initial_werner_parameter

    def apply_failing(self, connection_failure_probability=0):
        """Throws a random coin for each connection to determine
        whether to turn it to 'defect'. Connections which already
        were defect will remain so.

        Parameters
        ----------
        connection_failure_probability: float
        """
        for connection in self.connections:
            connection.apply_failing(
                failure_probability=connection_failure_probability)

    @property
    def status(self):
        """Returns a string of 'o' and 'x', e.g.
        'ooxxo' indicates the 3rd and 4th connection
        are defect.
        """
        string = ""
        for connection in self.connections:
            if connection.is_working:
                string += "o"
            else:
                string += "x"
        return string


class RepeaterChain:
    """Class modelling a chain of segments.

    Parameters
    ----------
    segments: list of :obj:`Segment`
    """

    def __init__(self, segments):
        self.segments = segments
        self._waiting_time = None

    @property
    def waiting_time(self):
        if self._waiting_time is None:
            raise Exception(
                "First call 'perform_new_round' on the repeater chain")
        else:
            return self._waiting_time

    def perform_new_round(self):
        for segment in self.segments:
            segment.perform_new_round()
        self._waiting_time = max([segment.waiting_time
                                  for segment in self.segments])

    def apply_failing(self, connection_failure_probability=0):
        for segment in self.segments:
            segment.apply_failing(
                connection_failure_probability=connection_failure_probability)

    @property
    def status(self):
        string = ""
        for segment in self.segments:
            string += segment.status
            string += " - "
        return string

    @property
    def werner_parameter(self):
        """The Werner parameter of the end-to-end link, computed from
        the Werner parameters of the individual elementary links before the
        entanglement swaps. Includes decoherence over time.
        """
        # Return the product of the individual werner parameters
        werner_parameter = 1
        for segment in self.segments:
            w = segment.werner_parameter(time=self.waiting_time)
            if w is None:
                return None
            else:
                werner_parameter *= w
        return werner_parameter

    @property
    def fidelity(self):
        """Bell-state fidelity of the end-to-end link.
        Includes decoherence over time.

        Note: Bell-state fidelity of the Werner state
        :math:`\rho(w) = w \Psi^- + (1 - w) Id_4`
        with :math:`\Psi^-` a Bell state
        equals :math:`(1 + 3 w) / 4`.
        """
        if self.werner_parameter is None:
            return None
        else:
            return (1 + 3 * self.werner_parameter) / 4


#######################################################
# METHODS FOR CREATING A SEGMENT AND A REPEATER CHAIN #
#######################################################

def create_segment(connection_attempt_success_probability,
                   connection_initial_failure_probability,
                   connection_failure_probability_per_timestep,
                   coherence_time,
                   number_of_connections_per_segment):
    """Create a segment and apply initial defects to its connections
    according to `connection_initial_failure_probability."""

    connections = []
    fail_prob = connection_failure_probability_per_timestep

    for __ in range(number_of_connections_per_segment):
        connection = Connection(
            attempt_success_probability=connection_attempt_success_probability,
            is_working=True,
            failure_probability_per_timestep=fail_prob,
            )
        connection.apply_failing(
            failure_probability=connection_initial_failure_probability)
        connections.append(connection)

    return Segment(connections=connections,
                   coherence_time=coherence_time)


def create_repeater_chain(connection_attempt_success_probability,
                          connection_initial_failure_probability,
                          number_of_connections_per_segment,
                          connection_failure_probability_per_timestep,
                          coherence_time,
                          number_of_segments):
    """Create a repeater chain and apply initial defects to its
    individual connections according to
    `connection_initial_failure_probability."""

    # shortening names so that lines don't become longer than 80 lines,
    # to comply with the PEP8 style of writing Python
    succ_prob = connection_attempt_success_probability
    initial_fail_prob = connection_initial_failure_probability
    variable_fail_prob = connection_failure_probability_per_timestep
    num_conns = number_of_connections_per_segment

    segments = [
        create_segment(
            connection_attempt_success_probability=succ_prob,
            connection_initial_failure_probability=initial_fail_prob,
            connection_failure_probability_per_timestep=variable_fail_prob,
            coherence_time=coherence_time,
            number_of_connections_per_segment=num_conns[__])
        for __ in range(number_of_segments)]

    repeater_chain = RepeaterChain(segments=segments)
    return repeater_chain


##################################################
# ENTERING PARAMETERS AND RUNNING THE SIMULATION #
##################################################

if __name__ == "__main__":

    # simulation parameters

    #number_of_segments = 5
    number_of_connections_per_segment = [3,3,3,3,3]
    elementary_link_attempt_success_probability = 0.01
    connection_initial_failure_probability = 0
    connection_failure_probability_per_timestep = 0.001
    coherence_time = 100  # timesteps
    number_of_simulation_rounds = 10

    # shortening names so that lines don't become longer than 80 lines,
    # to comply with the PEP8 style of writing Python
    succ_prob = elementary_link_attempt_success_probability
    initial_fail_prob = connection_initial_failure_probability
    variable_fail_prob = connection_failure_probability_per_timestep
    num_conns = number_of_connections_per_segment


    # create repeater chain from simulation parameters

    repeater_chain = \
        create_repeater_chain(
            connection_attempt_success_probability=succ_prob,
            connection_initial_failure_probability=initial_fail_prob,
            number_of_connections_per_segment=num_conns,
            connection_failure_probability_per_timestep=variable_fail_prob,
            coherence_time=coherence_time,
            number_of_segments=len(num_conns))

    # run simulation

    print('Repeater chain status before any runs:\t\t',
          repeater_chain.status,
          "(o=working connection, x=broken connection)",
          '\n')

    for round_number in range(1, number_of_simulation_rounds + 1):
        repeater_chain.perform_new_round()
        print('Round',
              round_number,
              'finished with repeater chain status:\t',
              repeater_chain.status)
        print('Waiting time: ', repeater_chain.waiting_time)
        print('End-end fidelity: ', repeater_chain.fidelity)
        print('')
